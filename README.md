# Static Code Analysis CLI Operation
This project is a LabVIEW Class that will run as Custom Operation using the NI LabVIEW CLI application.\
It is called Static Code Analysis which is in fact the VI Analyzer Task and it generates a report compatible with some Git Platforms, i.e. Gitlab.\
It also copies the custom tests from the folder (/test/static/custom) to LabVIEW Data Folder.

# Author
Felipe Pinheiro Silva

## Installation

To install you may use the NI Package Manager available in the [releases page](/releases).\
Additionally, it is possible use from the [docker images](https://hub.docker.com/r/felipefoz/vianalyzer) in Docker Hub.

## Dependencies
### Testing
This project uses Caraya Unit Test Framework.
### Packages
This project uses SDM Dependency Manager for downloading the needed libraries. Current needs:
-  gitlab-code-quality-labview : 0.2.0
-  cicd-tools.lvlib : 0.2.1

## Usage
Operation Name - **StaticCodeAnalysis**.\
Runs the specified VI analyzer task in the LabVIEW VI Analyzer Toolkit and saves the test report to the specified location.\
The following table lists the available arguments of this operation. Required arguments are in **bold**.

| Argument | Description | Default |
| :-------- | :----------- | :------- |
| **-ProjectPath** | 	Path to the project root directory. It assumes, if no -ConfigPath was passed, that the VI Analyzer task file will be in the test folder. | - |
| **−ReportPath** |	Path to the report or results file. |	- |
| −ConfigPath	| Path to the configuration file that contains VI Analyzer task settings to use in the analysis. You can use a configuration file you saved through the VI Analyzer or the VI Analyzer VIs. Alternatively, you can specify a VI, folder, or LLB to analyze. If you specify an item other than a configuration file, the VI runs all VI Analyzer tests on the specified item. | .viancfg in test folder |
| −ConfigPassword	| Password of the configuration file, if any.	| Empty string |
| −ReportSaveType	| Format of the report or results file. The value for this argument must be one of the following: JSON (Code Climate Format), ASCII, HTML, RSL | JSON |
| -Platform | If JSON is chosen, then it is possible to specify a Code Quality Report Type. Otherwise this parameter is ignored. This value must be one of the following available: Gitlab | Gitlab |

`LabVIEWCLI -OperationName StaticCodeAnalysis -ProjectPath <path to projet root directory> -ReportPath <path to report> -ConfigPath <path to configuration file> -ReportSaveType <file type of report> -ConfigPassword <password of configuration file>`

## Contributing
Merge requests are welcome. For major changes, please open an issue first to discuss what you would like to change.\
Please make sure to update tests as appropriate.

## License
[BSD3](LICENSE)
